# Changelog

## Unreleased

## 1.4.2 - 2025-02-14

### Fix

- #1 Pouvoir indiquer les versions compatibles uniquement avec la future branche
- Compatibilité SPIP 4.4

## 1.4.1 - 2024-09-30


### Fixed

- Ne pas tenir compte de la constante `_DEV_VERSION_SPIP_COMPAT`
## 1.4.0 - 2024-06-07
### Added

- !8 Indiquer la date de maj des dépôts
- Possibilité de tester pour SPIP 4.3
- #2 Possibilité de tester les plugins-dist non livrés avec SPIP
### Fixed

- #1 Indiquer les mises à jours proposables après la MAJ de SPIP
- #5 Ne pas proposer de tester avec les branches passées
- #4 Tester la compatibilité sur toute une branche de y, quel que soit le z min
- #6 Indiquer dans l'url la version testée
