<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-verifier_plugins?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// V
	'verifier_plugins_description' => 'Verifieer de compatibiliteit van je plugins voordat je SPIP naar een nieuwe versie opwaardeert',
	'verifier_plugins_nom' => 'Verifieer de compatibiliteit van jouw plugins',
	'verifier_plugins_slogan' => 'Verifieer de compatibiliteit van je plugins voordat je SPIP naar een nieuwe versie opwaardeert'
);
