<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/verifier_plugins?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'liste_plugins_inactifs' => 'List of inactive plugins',

	// P
	'plugin_borne' => 'Limits',
	'plugin_compat_version' => 'SPIP @version@ compatible ',
	'plugin_compat_version_maj' => 'The latest version of the plugin on the zone is compatible, please update your plugin.',
	'plugin_compat_version_maj_gestion' => 'Plugins management',
	'plugin_compat_version_notok' => 'The plugin currently installed is reported as incompatible or to be checked',
	'plugin_compat_version_ok' => 'OK',
	'plugin_nom' => 'Plugin name',

	// T
	'titre_verifier_plugins' => 'Check the compatibility of the plugins with another SPIP version',
	'titre_verifier_plugins_version' => 'Check plugin compatibility with SPIP @version@',

	// V
	'version_cible' => 'Display plugin compatibility for'
);
