<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-verifier_plugins?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// V
	'verifier_plugins_description' => 'Verifica a compatibilidade dos seus plugins antes de uma atualização de versão maior do SPIP',
	'verifier_plugins_nom' => 'Verifica a compatibilidade dos seus plugins',
	'verifier_plugins_slogan' => 'Verifica a compatibilidade dos seus plugins antes de uma atualização de versão maior do SPIP'
);
