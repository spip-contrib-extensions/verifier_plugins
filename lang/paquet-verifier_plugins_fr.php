<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/verifier_plugins.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// V
	'verifier_plugins_description' => 'Vérifie la compatiblité de vos plugins avant une mise à jour de version majeure de SPIP',
	'verifier_plugins_nom' => 'Vérifier la compatibilité de vos plugins',
	'verifier_plugins_slogan' => 'Vérifie la compatiblité de vos plugins avant une mise à jour de version majeure de SPIP'
);
