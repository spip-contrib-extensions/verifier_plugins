<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/verifier_plugins?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aller_maj' => 'Atualizar',

	// B
	'bilan_actifs' => '@total@ plugin(s) ativo(s) - @total_notok@ plugin(s) incompativel(is)',
	'bilan_inactifs' => '@total@ plugin(s) inativo(s) - @total_notok@ plugin(s) incompativel(is)',

	// D
	'date_actualisation' => 'Atualização das informações:',

	// L
	'liste_plugins_actifs' => 'Lista de plugins ativos',
	'liste_plugins_inactifs' => 'Lista dos plugins inativos',

	// P
	'plugin_borne' => 'Tolerância',
	'plugin_compat_version' => 'Compativel com o SPIP @version@',
	'plugin_compat_version_maj' => 'A versão mais recente do plugin disponível na zona é compatível, atualize o seu plugin',
	'plugin_compat_version_maj_gestion' => 'Gerenciamento dos plugins',
	'plugin_compat_version_maj_plus_tard' => 'Uma versão do plugin no repositório é compatível. Porém, ele só pode ser instalado após a atualização do SPIP.',
	'plugin_compat_version_notok' => 'O plugin instalado atualmente está marcado como incompatível ou a verificar',
	'plugin_compat_version_ok' => 'OK',
	'plugin_nom' => 'Nome do plugin',

	// T
	'titre_verifier_plugins' => 'Verificar a compatibilidade dos plugins com uma outra versão de SPIP',
	'titre_verifier_plugins_version' => 'Verificar a compatibilidade dos plugins com o SPIP @version@',

	// V
	'version_cible' => 'Exibir a compatibilidade dos plugins para'
);
