<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/verifier_plugins?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'liste_plugins_inactifs' => 'Lijst van inactieve plugins',

	// P
	'plugin_borne' => 'Limieten',
	'plugin_compat_version' => 'Compatibel met SPIP @version@',
	'plugin_compat_version_maj' => 'De meest recente versie is compatibel, pas de plugin aan.',
	'plugin_compat_version_maj_gestion' => 'Beheer van plugins',
	'plugin_compat_version_notok' => 'De momenteel geïnstalleerde plugin is ofwel niet-compatibel of moet worden gecontroleerd',
	'plugin_compat_version_ok' => 'OK',
	'plugin_nom' => 'Naam van de plugin',

	// T
	'titre_verifier_plugins' => 'Verifieer de compatibiliteit van plugins met een andere versie van SPIP',
	'titre_verifier_plugins_version' => 'Verifieer de compatibiliteit van plugins met SPIP @version@',

	// V
	'version_cible' => 'Toon de compatibiliteit van plugins voor'
);
