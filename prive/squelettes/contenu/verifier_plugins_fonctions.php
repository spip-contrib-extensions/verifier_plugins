<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// pour le filtre plugin_version_compatible
include_spip('inc/plugin');


/**
 * Prend la borne minimum
 * et redescend le z à 0
 * pour pouvoir tester toute une branch
 * @param string $intervalle
 * @return
 * @warning pas de typage, car on veut continuer à supporter SPIP 3.0, et donc PHP 5
**/
function verifier_plugin_normalise_version_min($intervalle) {
	if (!preg_match(_EXTRAIRE_INTERVALLE, $intervalle, $regs)) {
		return false;
	}

	// Extraction des bornes
	$minimum = $regs[1];
	$maximum = $regs[2];

	$version_min = explode('.', $minimum);

	if (count($version_min) === 3) {
		$version_min[2] = '0';
	}

	$minimum = implode('.', $version_min);

	$intervale = $intervalle[0] . $minimum . ';' . $maximum . substr($intervalle, -1);

	return $intervale;
}

/**
 * Extraire une branche depuis une version de SPIP
 * @param string $version
 * @return string
 * @warning pas de typage, car on veut continuer à supporter SPIP 3.0, et donc PHP 5
**/
function verifier_plugins_extraire_branche($version) {
	return preg_replace('/(\d*\.\d*).*/', '$1', $version);
}



/**
 * Va rechercher sur SVP API si le plugin est disponible pour une branche SPIP.
 * À n'utiliser qu'en derniers recours, car moins performant que la recherche via la base SVP locale.
 * @param string $prefixe
 * @param string $vspip
 * @return bool
**/
function verifier_compatibilite_via_API($prefixe, $vspip) {
	// Normalisation
	$branche_spip = verifier_plugins_extraire_branche($vspip);

	static $cache = '';

	if (!$cache) {
		$url = 'https://contrib.spip.net/http.api/ezrest/plugins';
		include_spip('inc/distant');
		// Préferer recuperer_url() à recuperer_page() qui n'existe pas sur les version récente de SPIP
		if (function_exists('recuperer_url')) {
			$contenu = recuperer_url($url);
			$contenu = $contenu['page'];
		} else {
			$contenu = recuperer_page($url);
		}
		$cache = json_decode($contenu, true);
	}

	if (isset($cache['donnees'][$prefixe]['branches_spip'])) {
		return in_array($branche_spip, $cache['donnees'][$prefixe]['branches_spip']);
	} else {
		return false;
	}
}


