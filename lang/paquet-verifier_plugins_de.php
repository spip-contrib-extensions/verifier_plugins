<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-verifier_plugins?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// V
	'verifier_plugins_description' => 'Überprüfen Sie die Kompatibilität Ihrer Plugins vor einem Update auf eine neue Version von SPIP.',
	'verifier_plugins_nom' => 'Prüfen Sie die Kompatibilität Ihrer Plugins.',
	'verifier_plugins_slogan' => 'Prüfen Sie die Kompatibilität Ihrer Plugins vor einem Update auf eine neue Version von SPIP.'
);
