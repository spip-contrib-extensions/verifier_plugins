<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/verifier_plugins?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'liste_plugins_inactifs' => 'Liste der deaktivierten Plugins',

	// P
	'plugin_compat_version' => 'Kompatibel mit SPIP @version@',
	'plugin_compat_version_maj' => 'Die neueste Version des Plugins auf SPIP-Zone ist kompatibel. Bitte führen Sie ein Update durch.',
	'plugin_compat_version_maj_gestion' => 'Plugin-Verwaltung',
	'plugin_compat_version_notok' => 'Das installierte Plugin ist inkompatibel oder muss überprüft werden.',
	'plugin_compat_version_ok' => 'OK',
	'plugin_nom' => 'Name des Plugins',

	// T
	'titre_verifier_plugins' => 'Prüfen Sie die Kompatibilität Ihrer Plugins mit einer anderen Version von SPIP',
	'titre_verifier_plugins_version' => 'Überprüfen Sie die Kompatibilität Ihrer Plugins mit SPIP @version@',

	// V
	'version_cible' => 'Anzeige der Kompatibilität der Plugins für'
);
